FROM debian
RUN apt-get update && apt-get install -y ca-certificates curl sudo
RUN curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update && apt-get install -y kubectl 
RUN apt-get update && \
    apt-get install -y gettext-base
RUN kubectl version --client --output=yaml    
ENTRYPOINT [ "kubectl" ]
CMD [ "--help" ]
